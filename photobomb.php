<?php

// Required Files
require('inc/config.php');
require('inc/functions.php');

// the upload path
$upload_path = getcwd().'/'.$config['upload_path'].'/';

// if this is a valid image
if(isset($_POST['fileAsString']))
{
	// check if directory is writeable
	if(is_writable($upload_path))
	{
		//create image from string
		$img = imagecreatefromstring(base64_decode(str_replace("data:image/png;base64,", "", $_POST['fileAsString'])));
		imagealphablending($img, true);
		imagesavealpha($img, true);

		// connect and login to FTP server
		$ftp_conn = ftp_connect($config['ftp_server']) or die("Could not connect to " . $config['ftp_server']);
		$login = ftp_login($ftp_conn, $config['ftp_username'], $config['ftp_userpass']);

		// set some stuff
		$photo_name = time() . '-' . mt_rand() . '.' . $config['photo_ext'];
		$img_hash = md5($photo_name);

		// build the directories from the hash
		$dir_1 = substr($img_hash, 0, 3);
		$dir_2 = substr($img_hash, 3, 3);
		$dir_3 = substr($img_hash, 6, 3);

		$hash_node[0] = $dir_1 . '/';
		$hash_node[1] = $hash_node[0] . $dir_2 . '/';
		$hash_node[2] = $hash_node[1] . $dir_3 . '/';

		// create the directories on the CDN
		foreach ($hash_node as $this_dir) {
			ftp_mksubdirs($ftp_conn,$config['ftp_root_dir'],$this_dir);
		}

		// The final Image name
		$final_img_name =  $img_hash .'.'.$config['photo_ext'];

		// Full Directory (Local)
		$final_img = $upload_path . $dir_1 . '/' . $dir_2 . '/'. $dir_3 . '/' . $final_img_name;

		// Full Directory (CDN)
		$ftp_image_loc = $config['ftp_root_dir'] . $dir_1 . '/' . $dir_2 . '/'. $dir_3 . '/' . $final_img_name;

		//JCG SAVE IMAGE
		if ($img != false) {

			// Create the PNG and Save the image
		    $theimg = imagepng($img, $upload_path . $final_img_name);

		    // Upload the image to the CDN
		    if(ftp_put($ftp_conn, $ftp_image_loc, $upload_path . $final_img_name, FTP_ASCII))
		    {
		    	// remove the image from the Temp directory after it was uploaded.
				unlink($upload_path . $final_img_name);
				ftp_close($ftp_conn);
		    }
		    else // Could not upload to the CDN.
		    {
		    	$data['status'] = 'error';
				$data['errors'] = array(
					'code' => 4,
					'desc' => 'Could not upload to the CDN.'
				);
		    }

			// build the JSON array success
			$data['status'] = 'success';
			$data['data'] = array(
				//'image_id' => $row['photo_id'],
				'image_path' => '/' . $dir_1 . '/' . $dir_2 . '/'. $dir_3 . '/' . $img_hash .'.'.$config['photo_ext']
			);
		}
		else // If the image could not be saved.
		{
			$data['status'] = 'error';
			$data['errors'] = array(
				'code' => 2,
				'desc' => 'Image could not be created'
			);
		}
	}
	else // If the upload directory is not writable.
	{
		$data['status'] = 'error';
		$data['errors'] = array(
			'code' => 3,
			'desc' => 'The Upload directory is not writable'
		);
	}

}
else // if no file was uploaded
{
	$data['status'] = 'error';
	$data['errors'] = array(
		'code' => 1,
		'desc' => 'No file was uploaded.'
	);
}

// display the JSON
$json = json_encode($data);
echo $json;

/* End Of File */